import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;

class UtilsTest {
    Utils utils = new Utils();

    @Test
    void testGetTermFrequency(){
        String emailBody = "Hi June, Hope you are doing well. You will be glad that I have decided to come to your place tomorrow. See you. Bye. Regards, August";

        var termFrequency = utils.getTermFrequency(emailBody);
        assertEquals(termFrequency.size(), 25);
    }

    @Test
    void testGetInverseDocumentFrequency(){
        HashSet<String> uniqueTerms = new HashSet<>(Arrays.asList("a", "is", "language", "experience", "the", "feature",
                "Music", "of", "miracle", "universal", "human"));

        HashMap<String, HashMap<String, Float>> allTermFrequency = new HashMap<>();

        HashMap<String, Float> tempMapOne = new HashMap<>();
        tempMapOne.put("a", (float) 0.2);
        tempMapOne.put("Music", (float) 0.2);
        tempMapOne.put("is", (float) 0.2);
        tempMapOne.put("language", (float) 0.2);
        tempMapOne.put("universal", (float) 0.2);

        allTermFrequency.put("1", tempMapOne);

        HashMap<String, Float> tempMapTwo = new HashMap<>();
        tempMapTwo.put("a", (float) 0.25);
        tempMapTwo.put("Music", (float) 0.25);
        tempMapTwo.put("miracle", (float) 0.25);
        tempMapTwo.put("is", (float) 0.25);

        allTermFrequency.put("2", tempMapTwo);

        HashMap<String, Float> tempMapThree = new HashMap<>();
        tempMapThree.put("the", (float) 0.11111111);
        tempMapThree.put("a", (float) 0.11111111);
        tempMapThree.put("feature", (float) 0.11111111);
        tempMapThree.put("Music", (float) 0.11111111);
        tempMapThree.put("of", (float) 0.11111111);
        tempMapThree.put("is", (float) 0.11111111);
        tempMapThree.put("experience", (float) 0.11111111);
        tempMapThree.put("universal", (float) 0.11111111);
        tempMapThree.put("human", (float) 0.11111111);

        allTermFrequency.put("3", tempMapThree);

        float numberOfTotalEmailDocuments = 3;


        var termsInverseDocumentFrequency = utils.getInverseDocumentFrequency(uniqueTerms, allTermFrequency, numberOfTotalEmailDocuments);

        assertEquals(termsInverseDocumentFrequency.size(), 11);

    }

    @Test
    void testGetTermFrequencyInverseDocumentFrequency(){
        HashSet<String> uniqueTerms = new HashSet<>(Arrays.asList("a", "is", "language", "experience", "the", "feature",
                "Music", "of", "miracle", "universal", "human"));

        HashMap<String, HashMap<String, Float>> allTermFrequency = new HashMap<>();

        HashMap<String, Float> tempMapOne = new HashMap<>();
        tempMapOne.put("a", (float) 0.2);
        tempMapOne.put("Music", (float) 0.2);
        tempMapOne.put("is", (float) 0.2);
        tempMapOne.put("language", (float) 0.2);
        tempMapOne.put("universal", (float) 0.2);

        allTermFrequency.put("1", tempMapOne);

        HashMap<String, Float> tempMapTwo = new HashMap<>();
        tempMapTwo.put("a", (float) 0.25);
        tempMapTwo.put("Music", (float) 0.25);
        tempMapTwo.put("miracle", (float) 0.25);
        tempMapTwo.put("is", (float) 0.25);

        allTermFrequency.put("2", tempMapTwo);

        HashMap<String, Float> tempMapThree = new HashMap<>();
        tempMapThree.put("the", (float) 0.11111111);
        tempMapThree.put("a", (float) 0.11111111);
        tempMapThree.put("feature", (float) 0.11111111);
        tempMapThree.put("Music", (float) 0.11111111);
        tempMapThree.put("of", (float) 0.11111111);
        tempMapThree.put("is", (float) 0.11111111);
        tempMapThree.put("experience", (float) 0.11111111);
        tempMapThree.put("universal", (float) 0.11111111);
        tempMapThree.put("human", (float) 0.11111111);

        allTermFrequency.put("3", tempMapThree);

        HashMap<String, Float> termsInverseDocumentFrequency = new HashMap<>();
        termsInverseDocumentFrequency.put("the", (float) 0.47712126);
        termsInverseDocumentFrequency.put("a", (float) 0.0);
        termsInverseDocumentFrequency.put("feature", (float) 0.47712126);
        termsInverseDocumentFrequency.put("Music", (float) 0.0);
        termsInverseDocumentFrequency.put("of", (float) 0.47712126);
        termsInverseDocumentFrequency.put("miracle", (float) 0.47712126);
        termsInverseDocumentFrequency.put("is", (float) 0.0);
        termsInverseDocumentFrequency.put("language", (float) 0.47712126);
        termsInverseDocumentFrequency.put("experience", (float) 0.47712126);
        termsInverseDocumentFrequency.put("universal", (float) 0.47712126);
        termsInverseDocumentFrequency.put("human", (float) 0.47712126);


        var termFrequencyInverseDocumentFrequency = utils.getTermFrequencyInverseDocumentFrequency(uniqueTerms, allTermFrequency, termsInverseDocumentFrequency);

        assertEquals(termFrequencyInverseDocumentFrequency.size(), 3);


    }

    @Test
    void testGetCosineSimilarity(){
        ArrayList<Float> documentOne = new ArrayList<>(Arrays.asList(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.119280316f, 0.0f, 0.0f));
        ArrayList<Float> documentTwo = new ArrayList<>(Arrays.asList(0.0f, 0.0f, 0.09542426f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.03521825f, 0.0f));

        var cosineSimilarity = utils.getCosineSimilarity(documentOne, documentTwo);

        assertEquals(cosineSimilarity, 0.0);

    }

    @Test
    void testGetMin(){
        List<Float> values = new ArrayList<>(Arrays.asList(0.0f, 0.0f, 0.09542426f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.03521825f, 0.0f));
        var min = utils.getMin(values);

        assertEquals(min,0.0);

    }

    @Test
    void testGetMax(){
        List<Float> values = new ArrayList<>(Arrays.asList(0.0f, 0.0f, 0.09542426f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.03521825f, 0.0f));
        var max = utils.getMax(values);

        assertEquals(max, 0.09542426f);

    }


}