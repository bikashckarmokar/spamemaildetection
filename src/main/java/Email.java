/**
 * <h1>Email</h1>
 * This defines the Email structure. Json file will be read as email object. Also, these email objects will be used
 * for all the operation and later when probability status will be updated then it will be written as json file.
 *
 * @author Bikash Chandra Karmokar.
 * @version 1.0.0
 */
public class Email {
    private String id;
    private String subject;
    private String body;
    private String spamProbability;

    /**
     * This function is used to get the id of the specific email object
     *
     * @return String This returns email id
     */
    public String getId(){
        return id;
    }

    /**
     * This function is used to get the subject of the specific email object
     *
     * @return String This returns email subject
     */
    public String getSubject(){
        return subject;
    }

    /**
     * This function is used to get the body of the specific email object
     *
     * @return String This returns email body text
     */
    public String getBody(){
        return body;
    }

    /**
     * This function set the probability status of specific email object.
     *
     * @param spamProbability probability of being spam
     */
    public void setSpamProbability(String spamProbability){
        this.spamProbability = spamProbability;
    }


}
