import java.text.DecimalFormat;
import java.util.*;

/**
 * <h1>Routine Task</h1>
 * This implements run function for scheduling a task by the routine.
 *
 * @author Bikash Chandra Karmokar.
 * @version 1.0.0
 */
public class RoutineTask extends TimerTask {

    /**
     * This function contains a set of instructions executes each time a task is scheduled by the routine.
     * Each task contains:
     * <ol>
     *     <li> Reading a set of emails </li>
     *     <li> Calculating term frequency (tf) for each document (email body text) </li>
     *     <li> Calculating inverse document frequency(idf) of each unique term from all the documents </li>
     *     <li> Calculating tf-idf vector for each document </li>
     *     <li> Calculating cosine similarities for each documents with all others</li>
     *     <li> Calculating probability of spam from the calculated cosine similarities </li>
     *     <li> Writing updated email set with probability as a json file</li>
     * </ol>
     */
    public void run() {
        Utils utils = new Utils();

        EmailReaderWriter emailReaderWriter = new EmailReaderWriter();
        // reading emails from json files
        var allEmails = emailReaderWriter.readEmails();

        HashMap<String, HashMap<String, Float>> allTermFrequency = new HashMap<String, HashMap<String, Float>>();

        float numberOfTotalEmailDocuments = 0;
        ArrayList<String> allTerms = new ArrayList<>();

        // calculating tf (term frequencies) of all the documents
        // also counting number of total documents
        for (Email email : allEmails) {
            var emailId = email.getId();
            var emailBody = email.getBody();

            HashMap<String, Float> termFrequency = utils.getTermFrequency(emailBody);

            var terms = termFrequency.keySet();
            allTerms.addAll(terms);

            allTermFrequency.put(emailId, termFrequency);
            numberOfTotalEmailDocuments += 1;
        }

        // unique words/terms from all the documents are filtered here
        HashSet<String> uniqueTerms = new HashSet<>(allTerms);

        // calculating idf - inverse document frequency
        var termsInverseDocumentFrequency = utils.getInverseDocumentFrequency(uniqueTerms, allTermFrequency, numberOfTotalEmailDocuments);

        // calculating tf-idf vectors for each document
        var termFrequencyInverseDocumentFrequency = utils.getTermFrequencyInverseDocumentFrequency(uniqueTerms, allTermFrequency, termsInverseDocumentFrequency);

        HashMap<String, ArrayList<Float>> emailsCosineSimilarity = new HashMap<>();

        // calculating cosine similarities for each document with all others and keeping it in a hashmap.
        // document id as key and all the similarities with other documents as list of values.
        // so later from the similarities we can find maximum cosine similarity or difference between maximum and
        // minimum cosine similarity of a given document with others
        for (String eidOuter : termFrequencyInverseDocumentFrequency.keySet()) {

            for (String eidInner : termFrequencyInverseDocumentFrequency.keySet()) {
                if (!eidInner.equals(eidOuter)) {
                    var eidInnerDocument = termFrequencyInverseDocumentFrequency.get(eidInner);
                    var eidOuterDocument = termFrequencyInverseDocumentFrequency.get(eidOuter);


                    // calling cosine similarity function with two documents
                    float cosineSimilarity = utils.getCosineSimilarity(eidInnerDocument, eidOuterDocument);

                    ArrayList<Float> documentOneCosineSimilarities;
                    ArrayList<Float> documentTwoCosineSimilarities;

                    // if the email id is already in the hashmap then it will update the list
                    // if it is new then it will add a list with the value inside
                    if (emailsCosineSimilarity.containsKey(eidInner)) {
                        documentOneCosineSimilarities = emailsCosineSimilarity.get(eidInner);
                        documentOneCosineSimilarities.add(cosineSimilarity);
                    } else {
                        documentOneCosineSimilarities = new ArrayList<>(Arrays.asList(cosineSimilarity));
                    }
                    emailsCosineSimilarity.put(eidInner, documentOneCosineSimilarities);

                    if (emailsCosineSimilarity.containsKey(eidOuter)) {
                        documentTwoCosineSimilarities = emailsCosineSimilarity.get(eidOuter);
                        documentTwoCosineSimilarities.add(cosineSimilarity);
                    } else {
                        documentTwoCosineSimilarities = new ArrayList<>(Arrays.asList(cosineSimilarity));
                    }
                    emailsCosineSimilarity.put(eidOuter, documentTwoCosineSimilarities);

                }
            }

        }

        // Now we have cosine similarities for all the documents. So for each document we have cosine similarity values
        // with all others documents. if the difference between minimum cosine similarity and maximum cosine similarity
        // of one document with others is 0 then all are similar documents.If the difference is not 0 then it is more
        // similar to the document with which it has maximum cosine similarity value and that is taken as spam
        // probability.
        for (Email email : allEmails) {
            var emailId = email.getId();
            var cosineSimilarity = emailsCosineSimilarity.get(emailId);


            float min = utils.getMin(cosineSimilarity);
            float max = utils.getMax(cosineSimilarity);
            float rangeMinMax= max-min;

            float probability;

            if(rangeMinMax == 0){
                probability = 1;
            }else{
                probability = utils.getMax(cosineSimilarity);
            }
            probability = probability * 100;
            DecimalFormat df = new DecimalFormat("0.00");
            var probabilityString = df.format(probability) + "%";

            email.setSpamProbability(probabilityString);
            System.out.println("Spam probability of email id " + emailId + " is " + probabilityString + " in file " + emailReaderWriter.getFileName() + ". Please check updated file in output folder.");

        }

        // writing updated email objects to json file with probabilities of spam
        emailReaderWriter.writeEmails(allEmails);

    }
}
