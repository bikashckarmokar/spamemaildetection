import java.util.*;

/**
 * <h1>Utils</h1>
 *
 * This class contains all the required functions to complete a task.
 *
 * @author Bikash Chandra Karmokar.
 * @version 1.0.0
 */
public class Utils {

    /**
     * This function calculates term frequency of the given text. The number of times a term occurs in a document is
     * used as its term frequency here. Stop words are removed here from the text before calculating term frequency.
     * There are many stop words in English language. Only few of them are added here for simplicity.
     *
     * Term frequency is calculated following the below equation:
     * <p>
     * tf ( t, d ) = n / N, where
     * <ul>
     *       <li> tf is the term frequency function </li>
     *       <li> t is the term/ word </li>
     *       <li> d is the document </li>
     *       <li> n is the number of occurrences of t in d </li>
     *       <li> N is the number of occurrences of t in all documents </li>
     * </ul>
     * </p>
     *
     * <p>For example we can consider the text: "Space is a miracle in our universe". If we remove stop words(is, a, in)
     * we will get "Space miracle our universe". So now we have total 4 words and each word occurs exactly 1 time.
     * So term frequency of each word will be space: 1/4, miracle: 1/4, our: 1/4, universe: 1/4</p>
     *
     * @param document This is the text of the email body.
     * @return String This returns term frequency of the document.
     */
    public HashMap<String, Float> getTermFrequency(String document) {
        String[] stopWords = {"a","an", "the", "another", "for", "nor", "but", "or", "yet", "so", "in", "on", "under",
        "towards", "before", "after", "about", "as", "at"};

        String[] words = document.split(" ");

        // removing stop words('a', 'an' etc.) from given text which is words array here
        for(String item: stopWords){
            words = Arrays.stream(words).filter(s -> !s.equals(item)).toArray(String[]::new);
        }

        HashMap<String, Integer> termCount = new HashMap<>();

        float numberOfTotalWords = 0;

        // counting how many times a word is present in the text
        for (String word : words) {
            numberOfTotalWords += 1;

            if (termCount.containsKey(word)) {
                Integer count = termCount.get(word);
                termCount.put(word, count + 1);
            } else {
                termCount.put(word, 1);
            }
        }

        HashMap<String, Float> termFrequency = new HashMap<>();

        // calculating term frequency of a word by dividing its number of occurrence
        // with the number of total available words
        for (String term : termCount.keySet()) {
            float frequency = termCount.get(term) / numberOfTotalWords;
            termFrequency.put(term, frequency);
        }

        return termFrequency;

    }

    /**
     * This function will calculate inverse document frequency of all the unique words of all the available documents.
     * This signifies how commonly a certain term occurs in all the documents.
     *
     * <p>
     * Consider below 3 sentences as separate documents.
     * <ul>
     *     <li> Music is a universal language </li>
     *     <li> Music is a miracle </li>
     *     <li> Music is a universal feature of the human experience </li>
     * </ul>
     *
     * Now if we calculate idf of words of these documents then "Music" will have lowest idf value wheres "experience" will
     * have highest idf value. Because Music presents in all the documents and experience presents only in one document.
     * </p>
     *
     * Inverse document frequency is calculated following the below equation:
     * <p>
     *     idf ( t, d ) = log ( D / { d ∈ D : t ∈ d }), where
     *     <ul>
     *         <li> idf is the inverse document frequency </li>
     *         <li> t is the term/ word </li>
     *         <li> d is the document </li>
     *         <li> D is the total number of documents </li>
     *         <li> { d ∈ D : t ∈ d } denotes the number of documents in which t occur </li>
     *     </ul>
     * </p>
     *
     *
     *
     * @param uniqueTerms This contains all unique words from all the documents/emails
     * @param allTermFrequency This contains term frequencies of all the documents/emails
     * @param numberOfTotalEmailDocuments This is the number of total available document/emails
     * @return HashMap This returns inverse document frequency(IDF) of each unique term
     */
    public HashMap<String, Float> getInverseDocumentFrequency(HashSet<String> uniqueTerms,
                                                              HashMap<String, HashMap<String, Float>> allTermFrequency,
                                                              float numberOfTotalEmailDocuments) {

        HashMap<String, Float> termsInverseDocumentFrequency = new HashMap<>();

        for (String term : uniqueTerms) {
            float presentInTotalDocuments = 0;

            // finding the number of documents where "term" presents.
            for (String eid : allTermFrequency.keySet()) {
                var tf = allTermFrequency.get(eid);
                boolean present = tf.containsKey(term);
                if (present) {
                    presentInTotalDocuments += 1;
                }
            }

            // calculating idf for "term" by dividing number of total documents with the number of documents
            // where "term" presents.
            float idf = (float) Math.log10(numberOfTotalEmailDocuments / presentInTotalDocuments);

            termsInverseDocumentFrequency.put(term, idf);
        }

        return termsInverseDocumentFrequency;
    }

    /**
     * This function calculates the product of term frequency(tf) and inverse document frequency(idf) for each unique term
     * per documents. The logarithmic term in idf approaches zero for a term presents in more numbers of documents, from
     * all the documents under consideration. Thus, the tf-idf value for a more common term approaches zero.
     *
     * Term Frequency-InverseDocument Frequency (tf-idf) is calculated following the below equation:
     * <p>
     *     tf-idf ( t, d, D ) = tf ( t, d ) * idf ( t, D ), where
     *     <ul>
     *         <li> tf-idf is the term frequency - inverse document frequency </li>
     *         <li> t is the term/ word </li>
     *         <li> d is the document </li>
     *         <li> D is the total number of documents </li>
     *         <li> tf ( t, d ) is the term frequency </li>
     *         <li> idf ( t, D ) is the inverse document frequency </li>
     *     </ul>
     * </p>
     *
     * @param uniqueTerms This contains all unique words from all the documents/emails
     * @param allTermFrequency This contains term frequencies of all the documents/emails
     * @param termsInverseDocumentFrequency This contains inverse document frequency(IDF) of each unique term
     * @return HashMap This returns tf-idf vector for each document
     */
    public HashMap<String, ArrayList<Float>> getTermFrequencyInverseDocumentFrequency(HashSet<String> uniqueTerms,
                                                                                      HashMap<String, HashMap<String, Float>> allTermFrequency,
                                                                                      HashMap<String, Float> termsInverseDocumentFrequency) {

        HashMap<String, ArrayList<Float>> termFrequencyInverseDocumentFrequency = new HashMap<>();

        // looping through all the available document's term frequencies.
        // here eid is the document/email id
        for (String eid : allTermFrequency.keySet()) {
            ArrayList<Float> tfidfPerDocument = new ArrayList<>();

            var termFreq = allTermFrequency.get(eid);

            // calculating tf-idf vector for each document
            // all the unique term from all documents are taken into consideration
            for (String term : uniqueTerms) {
                boolean present = termFreq.containsKey(term);

                // if the "term" not present in the current document then its value set to 0
                float tf;
                if (present) {
                    tf = termFreq.get(term);
                } else {
                    tf = 0;
                }

                var idf = termsInverseDocumentFrequency.get(term);

                float tfidf = tf * idf;
                tfidfPerDocument.add(tfidf);
            }

            // tf-idf vector for each document is stored here
            termFrequencyInverseDocumentFrequency.put(eid, tfidfPerDocument);
        }

        return termFrequencyInverseDocumentFrequency;
    }

    /**
     * This function calculates cosine similarity between two documents by measuring cosine of angle between two
     * tf-idf vectors derived from each of the documents.
     *
     * The cosine similarity can be found by taking the Dot Product of the document vectors following belo equation:
     *
     * <p>
     *     cosθ = v1.v2 / ( |v1| * |v2| ), where
     *     <ul>
     *         <li> cosθ is cosine similarity </li>
     *         <li> v1.v2 is dot product </li>
     *         <li> |v1| is magnitude of v1 </li>
     *         <li> |v2| is magnitude of v2 </li>
     *     </ul>
     * </p>
     *
     * For example, if v1 = [ 1 3 2 ] and v2 = [ 5 0 -3], then the dot product of v1 and v2 will be:
     * v1 . v2 = 1 * 5 + 3 * 0 + 2 * -3 = 5 + 0 + -6 = -1
     * and magnitude will be:
     * |v1| = sqrt(1^2 + 3^2 + 2^2 = 1 + 9 + 4) = 3.74
     * and
     * |v2| = sqrt(5^2 + 0^2 + (-3)^2 = 25 + 0 + 9) = 5.83
     * Finally, cosine similarity = -1/(3.74 * 5.83)
     *
     * @param documentOne tf-idf vector of document one
     * @param documentTwo tf-idf vector of document two
     * @return float This returns cosine similarity of two documents
     */
    public float getCosineSimilarity(ArrayList<Float> documentOne, ArrayList<Float> documentTwo){
        float dotProduct = 0;
        float docOneSquarePlus = 0;
        float docTwoSquarePlus = 0;

        // calculating dot product of two documents and also
        // taking power of each values from two documents and adding in separate variables for finding magnitude later
        for (int i=0; i < documentOne.size(); i++){
            float docOneValue = documentOne.get(i);
            float docTwoValue = documentTwo.get(i);

            float product = docOneValue * docTwoValue;
            dotProduct += product;

            docOneSquarePlus += Math.pow(docOneValue, 2);
            docTwoSquarePlus += Math.pow(docTwoValue, 2);

        }

        float cosineSimilarity;

        // calculating magnitude of doc1 and doc2
        float docOneMagnitude = (float)Math.sqrt(docOneSquarePlus);
        float docTwoMagnitude = (float)Math.sqrt(docTwoSquarePlus);

        // calculating cosine similarity
        if (dotProduct == 0){
            cosineSimilarity = 0;
        }else {
            cosineSimilarity = dotProduct / (docOneMagnitude * docTwoMagnitude);
        }

        return cosineSimilarity;
    }


    /**
     * This function finds min value of a given list.
     *
     * @param values values to find min
     * @return float This returns min value
     */
    public float getMin(List<Float> values){
        float min = Float.MAX_VALUE;

        for (Float i : values) {
            if (min > i) {
                min = i;
            }
        }

        return min;
    }

    /**
     * This function finds max value of a given list
     *
     * @param values values to find max
     * @return float This returns max value
     */
    public float getMax(List<Float> values){
        float max = Float.MIN_VALUE;

        for (Float i : values) {
            if (max < i) {
                max = i;
            }
        }

        if (max == Float.MIN_VALUE) return 0;

        return max;
    }
}
