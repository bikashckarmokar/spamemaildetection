import com.google.gson.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

/**
 * <h1>Read and Write Email Documents</h1>
 * This implements functions for reading and writing json file.
 *
 * @author Bikash Chandra Karmokar.
 * @version 1.0.0
 */
public class EmailReaderWriter {

    // initialized Gson object with pretty printing option.
    // during writing email object to json file pretty print will format the json file for easy reading
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private String fileName;

    public String getFileName(){
        return this.fileName;
    }

    /**
     * This function reads emails from json file. As there are only 3 sample json files so random value is generated
     * to take one json file randomly on each run.
     *
     * @return This returns an array of Email objects.
     */
    public Email[] readEmails() {

        Random rand = new Random();

        int fileNumber = rand.nextInt(3);


        this.fileName = "hamPlusSpamEmails.json";

        if (fileNumber == 0) {
            this.fileName = "hamPlusSpamEmails.json";
        } else if (fileNumber == 1) {
            this.fileName = "spamEmails.json";
        } else if (fileNumber == 2) {
            this.fileName = "hamEmails.json";
        }

        String file = "input/" + this.fileName;

        Path path = Paths.get(file);

        Email[] allEmails = new Email[0];

        try (Reader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {

            JsonArray emailJson = JsonParser.parseReader(reader).getAsJsonArray();


            // mapped json array to Email class
            // all emails will be in allEmails array as separate Email objects
            allEmails = gson.fromJson(emailJson, Email[].class);

        } catch (Exception e) {
            System.out.println("Could not read json file.");
        }

        return allEmails;
    }

    /**
     * This function writes all the email objects as a json file. In the newly written file the probability status
     * will be updated with the probability of being spam.
     *
     * @param allEmails This contains all the emails with updated probability status.
     */
    public void writeEmails(Email[] allEmails){
        String file = "output/" + this.fileName;
        Path path = Paths.get(file);

        try (FileWriter writer = new FileWriter(String.valueOf(path))) {
            gson.toJson(allEmails, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
