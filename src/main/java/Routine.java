import java.util.Timer;

/**
 * <h1>Routine for Spam Email Detection</h1>
 *
 * This routine will take a set of emails and assign a spam probability to each email. It will determine spam
 * probability based on the similarity of the email body texts. The more similar the text of the selected
 * emails, the higher the probability of being spam.
 *
 * <p>
 *
 * @author Bikash Chandra Karmokar.
 * @version 1.0.0
 */
public class Routine {
    /**
     *This is the starting point of the routine. Every 3 seconds it will start a task.
     *
     * @param args This is not used.
     */
    public static void main(String[] args){
        Timer time = new Timer();
        RoutineTask routineTask = new RoutineTask();
        time.schedule(routineTask, 0, 3000);
    }
}
