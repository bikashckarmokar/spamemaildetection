# Spam Email Detection
A routine will run each 3 seconds and take a bunch of emails from the JSON files. Then it will update the spam 
probability and save in another file in the output folder.


# Demo
![Image_Caption](SpamEmailDetection.gif)



# References
1. [Find similarity between documents using TF IDF](https://iq.opengenus.org/document-similarity-tf-idf/)

2. [Understanding TF IDF](https://iq.opengenus.org/tf-idf/)
 
3. [How important the words in your text data?](https://towardsdatascience.com/how-important-are-the-words-in-your-text-data-tf-idf-answers-6fdc733bb066)

4. [TF(Term Frequency)-IDF(Inverse Document Frequency)](https://towardsdatascience.com/tf-term-frequency-idf-inverse-document-frequency-from-scratch-in-python-6c2b61b78558)
